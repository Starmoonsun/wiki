# Just clone&commit [= Install Mkdocs =]
![mkdocs](images/1.jpeg)
## You need Python and pip.
For mac mini in our lab:

*  `1.pip install mkdocs`
*  `2.mkdocs --version`
*  `3.mkdocs new testwiki`
*  `4.cd testwiki`

* `![mkdocs](images/mkdocs.png)`
* `pip install mkdocs-material`
* `theme:`
 ` name: 'material'`

Now，you just need git clone this project.
You can add your markdown file in `docs` (I suggest you can creat a new filepackage so that we can read more eaier).
Then, you just need add the path of markdown file in `mkdocs.yml`.
Ctrl+s, then commit.
Your update will be shown on the website.
Cool!

 docker run -d --name gitlab-runner --restart always \
  -v /Users/Shared/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest


  
# Welcome to MkDocs

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

## Readme
<div style="text-align:justify;text-justify:inter-ideograph">

Project layout

    mkdocs.yml   # The configuration file. You can update navigation directory in `nav` part.
    docs/
        index.md          # The documentation homepage.
        folders/...       # Other markdown pages, images and other files.
For MacOS:

* `10.8.6.114 rioslab.rios-gitlab-6-114` - Add this line for accessing Wiki in Lab network env.
* `git clone https://` - Clone this project.
* Now, you can write your document. `Wiki/docs` is the path for markdown files and image files.
You can update them in the directory of your/your group project. At present, I have designed the directory names of some projects, such as `docs/Picorio`. If you have a new project, just create a new folder.
* You can use `![mkdocs](images/xxxx.jpeg)` for adding an image for your docs. For mor infomation, you can visit [mkdocs.org](https://www.mkdocs.org).
* When you finish your article, you need update the `madocs.yml` for navigation directory.
* Last, you just need issue your update. `git push` is OK. After a little minutes, you can visit it in Wiki page.
</div>
